#!/bin/bash
cat << EOF > /etc/sysconfig/i18n
LANG=ru_RU.UTF-8
SUPPORTED=ru_RU
EOF
postgres_conf=/var/lib/pgsql/data/postgresql.conf
pg_hba_conf=/var/lib/pgsql/data/pg_hba.conf
backup_dir=/var/lib/pgsql/backup/
if ! [ -f ${postgres_conf} ]; then
chown -R postgres:postgres /var/lib/pgsql/data/
su -s /bin/sh -l postgres -c "/usr/bin/initdb --data-checksums --encoding=UTF-8 --locale=ru_RU.UTF-8 -D /var/lib/pgsql/data/"
cat << EOF > /var/lib/pgsql/data/pg_hba.conf
local   all             all                                     trust
host    all             all             127.0.0.1/32            trust
host	all		all		0.0.0.0/0		md5
host    all             all             ::1/128                 trust
local   replication     all                                     trust
host    replication     all             127.0.0.1/32            trust
host    replication     all             ::1/128                 trust
EOF
sed -i -e 's/#listen_addresses = 'localhost'/#listen_addresses = '*'/' -e 's/#archive_mode = off/archive_mode = on/' -e "s/#archive_command = ''/archive_command = '/usr/bin/pg_probackup-14 archive-push -B ${backup_dir} --instance buh_2022_05 --wal-file-path=%p --wal-file-name=%f'" ${postgres_conf}
fi
chown -R postgres:postgres /var/lib/pgsql/data/
/etc/init.d/postgresql start
while true
do
	sleep 90
done
