FROM alt:sisyphus
COPY ./build/* /root/
run /root/install_pkg.sh
EXPOSE 5432
CMD ["/root/run.sh"]
